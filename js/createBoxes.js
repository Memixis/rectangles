import config from './config.js';
import v from './variables.js';

function createBoxes() {
    let spaceY = 0;

    for (let i = 0; i < config.boxesCount; i++) {
        const [x, y] = [config.boxSize[Math.ceil(Math.random() * config.boxSize.length) - 1], config.boxSize[Math.ceil(Math.random() * config.boxSize.length) - 1]];
        const newBox = {
            x: config.xSpacing,
            y: i === 0 ? 0 : spaceY,
            width: x,
            height: y,
            color: config.defaultColor,
            collided: false,
            snap: false,
            snappingBorder: {
                x: config.xSpacing - config.snappingDistance,
                y: i === 0 ? -config.snappingDistance : spaceY - config.snappingDistance,
                width: x + config.snappingDistance * 2,
                height: y + config.snappingDistance * 2
            }
        };

        spaceY += y + config.ySpacing;
        v.boxes.push(newBox);
    }
}

export default {
    createBoxes
}