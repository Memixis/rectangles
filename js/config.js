export default {
    boxesCount: 5,
    boxSize: [50, 75, 100, 125, 150, 175, 200],
    defaultColor: '#d4edda',
    collisionColor: '#f8d7da',
    strokeWidth: 1,
    strokeColor: '#000',
    ySpacing: 30,
    xSpacing: 30,
    snappingDistance: 5
}