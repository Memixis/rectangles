import config from './config.js';
import v from './variables.js';

function searchForCollision() {
    v.boxes.forEach((e, i) => {
        if (e.x < v.currentBox.x + v.currentBox.width &&
            e.x + e.width > v.currentBox.x && e.y < v.currentBox.y + v.currentBox.height &&
            e.y + e.height > v.currentBox.y &&
            i !== v.currentId) {
            v.currentBox.collided = true;
            setCollisionAndColor(v.boxes[i], 'collisionColor');
            if (!v.collisionBoxes.includes(i)) v.collisionBoxes.push(i);
        } else if (v.collisionBoxes.length && v.collisionBoxes.includes(i)) {
            v.collisionBoxes.splice(v.collisionBoxes.indexOf(i), 1);
            setCollisionAndColor(v.boxes[i], 'defaultColor');
        }
    });
}

const setCollisionAndColor = (collision, color) => {
    collision.color = config[color];
    if (!v.collisionBoxes.length) {
        v.currentBox.color = config[color];
        v.currentBox.collided = false;
    }
}

export default {
    searchForCollision
}