import config from './config.js';
import v from './variables.js';

function setCurrentBoxXY(x, y) {
    v.currentBox.x = x;
    v.currentBox.y = y;
}

function setCurrentBoxSnappingDistance(x, y) {
    v.currentBox.snappingBorder.x = x - config.snappingDistance;
    v.currentBox.snappingBorder.y = y - config.snappingDistance;
}

function saveBoxParameters(i, box) {
    v.currentBox = v.boxes[i];
    v.currentId = i;
    v.savedPosition = [box.x, box.y];
    v.movePositionX = box.x;
    v.movePositionY = box.y;
}

export default {
    setCurrentBoxXY,
    setCurrentBoxSnappingDistance,
    saveBoxParameters
}