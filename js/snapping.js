import canvas from './canvas.js';
import collision from './collision.js';
import m from './methods.js';
import v from './variables.js';

let mouseDirection;

function searchForSnapping() {
    if (!v.currentBox.snap && !v.currentBox.collided) {
        for (let i = 0; i < v.boxes.length; i++) {
            let e = v.boxes[i].snappingBorder,
                current = v.currentBox.snappingBorder;

            if (e.x < current.x + current.width &&
                e.x + e.width > current.x &&
                e.y < current.y + current.height &&
                e.y + e.height > current.y &&
                i !== v.currentId) {
                v.currentBox.snap = true;
                v.snapped = true;
                snapWithOtherBox(v.boxes[i]);
                break;
            }
        };
    }
}

const snapWithOtherBox = box => {
    mouseDirection = v.mouseDirectionX === "right";
    let distances = calculateDistance(box, mouseDirection);
    const closestPoint = distances.indexOf(Math.min(...distances));

    setToClosestPoint(box, closestPoint);

    v.snapPositionX = v.currentBox.x;
    v.snapPositionY = v.currentBox.y;

    m.setCurrentBoxSnappingDistance(v.currentBox.x, v.currentBox.y);
    collisionAndClear();

    if (v.collisionBoxes.length) {
        collisionAndClear();
        v.snapped = false;
    } else {
        v.snapped = false;
    }
}

const calculateDistance = (box, mouseDirection) => {
    const currentBoxHeightAdd = v.currentBox.y + v.currentBox.height,
        currentBoxWidthAdd = v.currentBox.x + v.currentBox.width,
        boxHeightAdd = box.y + box.height,
        boxWidthAdd = box.x + box.width;

    return [
        Math.sqrt((box.x - v.currentBox.x) ** 2 + (box.y - currentBoxHeightAdd) ** 2),
        Math.sqrt((boxWidthAdd - currentBoxWidthAdd) ** 2 + (box.y - currentBoxHeightAdd) ** 2),

        mouseDirection ? Math.sqrt((boxWidthAdd - v.currentBox.x) ** 2 + (box.y - v.currentBox.y) ** 2) :
        Math.sqrt((box.x - currentBoxWidthAdd) ** 2 + (box.y - v.currentBox.y) ** 2),
        mouseDirection ? Math.sqrt((boxWidthAdd - v.currentBox.x) ** 2 + (boxHeightAdd - currentBoxHeightAdd) ** 2) :
        Math.sqrt((box.x - currentBoxWidthAdd) ** 2 + ((box.y + box.height) - currentBoxHeightAdd) ** 2),

        Math.sqrt((box.x - v.currentBox.x) ** 2 + (boxHeightAdd - v.currentBox.y) ** 2),
        Math.sqrt((boxWidthAdd - currentBoxWidthAdd) ** 2 + (boxHeightAdd - v.currentBox.y) ** 2),
    ]
}

const setToClosestPoint = (box, closestPoint) => {
    switch (closestPoint) {
        case 0:
            m.setCurrentBoxXY(box.x, box.y - v.currentBox.height);
            break;
        case 1:
            m.setCurrentBoxXY(box.x + box.width - v.currentBox.width, box.y - v.currentBox.height);
            break;
        case 2:
            m.setCurrentBoxXY(mouseDirection ? box.x + box.width : box.x - v.currentBox.width,
                mouseDirection ? box.y : box.y);
            break;
        case 3:
            m.setCurrentBoxXY(mouseDirection ? box.x + box.width : box.x - v.currentBox.width,
                mouseDirection ? box.y + box.height - v.currentBox.height : box.y + box.height - v.currentBox.height);
            break;
        case 4:
            m.setCurrentBoxXY(box.x, box.y + box.height);
            break;
        case 5:
            m.setCurrentBoxXY(box.x + box.width - v.currentBox.width, box.y + box.height);
            break;
    }
}

const collisionAndClear = () => {
    collision.searchForCollision();
    canvas.clearCanvas();
    canvas.drawBoxesOnCanvas();
}

export default {
    searchForSnapping
}