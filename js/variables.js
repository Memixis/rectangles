export default {
    currentId: 0,
    movePositionX: 0,
    movePositionY: 0,
    snapPositionX: 0,
    snapPositionY: 0,
    mouseDirectionX: '',
    snapped: false,
    boxes: [],
    collisionBoxes: [],
    savedPosition: [],
    currentBox: {
        collided: false
    }
}