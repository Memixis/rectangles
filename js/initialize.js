import canvas from './canvas.js';

document.addEventListener('DOMContentLoaded', function(e) {
    canvas.initializeCanvas();
});

window.addEventListener('resize', function(e) {
    canvas.setCanvasHeight();
    canvas.clearCanvas();
    canvas.drawBoxesOnCanvas();
});