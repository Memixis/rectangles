import config from './config.js';
import create from './createBoxes.js';
import events from './events.js';
import v from './variables.js';

let canvas, ctx, rect;

function initializeCanvas() {
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');

    setCanvasHeight();
    addEventsToCanvas();
    create.createBoxes();
    drawBoxesOnCanvas();
}

function setCanvasHeight() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}

function addEventsToCanvas() {
    canvas.onmousedown = events.tryToCatchBox;
    canvas.onmouseup = events.tryToDropBox;
    canvas.onmousemove = events.moveBox;
}

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function drawBoxesOnCanvas() {
    v.boxes.forEach((e, i) => {
        ctx.beginPath();
        ctx.rect(e.x, e.y, e.width, e.height);
        ctx.fillStyle = e.color;
        ctx.fill();
        ctx.lineWidth = config.strokeWidth;
        ctx.strokeStyle = config.strokeColor;
        ctx.stroke();
        ctx.closePath();
    });
}

export default {
    initializeCanvas,
    setCanvasHeight,
    clearCanvas,
    drawBoxesOnCanvas
}