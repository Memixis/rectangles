import canvas from './canvas.js';
import collision from './collision.js';
import config from './config.js';
import snap from './snapping.js';
import m from './methods.js';
import v from './variables.js';

let mouseX,
    mouseY,
    differenceX = 0,
    differenceY = 0,
    cursorMoveX = 0,
    cursorMoveY = 0,
    dragging = false;

function tryToCatchBox(e) {
    if (!dragging) {
        for (let i = 0; i < v.boxes.length; i++) {
            const box = v.boxes[i];
            if (mouseX > box.x && mouseX < box.x + box.width && mouseY > box.y && mouseY < box.y + box.height) {
                dragging = true;
                m.saveBoxParameters(i, box);
                break;
            }
        }
    }
}

function tryToDropBox(e) {
    dragging = false;

    if (v.currentBox.collided) {
        const [x, y] = [...v.savedPosition];

        m.setCurrentBoxXY(x, y);
        m.setCurrentBoxSnappingDistance(x, y);

        collision.searchForCollision();
        canvas.clearCanvas();
        canvas.drawBoxesOnCanvas();
    }
}

function moveBox(e) {
    v.mouseDirectionX = mouseX < e.pageX ? "left" : "right";

    differenceX = e.pageX - mouseX;
    differenceY = e.pageY - mouseY;

    if (dragging && !v.currentBox.snap) {
        v.movePositionX += differenceX;
        v.movePositionY += differenceY;

        m.setCurrentBoxXY(v.movePositionX, v.movePositionY);
        m.setCurrentBoxSnappingDistance(v.movePositionX, v.movePositionY);

        canvas.clearCanvas();
        canvas.drawBoxesOnCanvas();
        collision.searchForCollision();
        if (!v.snapped) {
            snap.searchForSnapping();
        }

    } else if (v.currentBox.snap && dragging) {
        cursorMoveX += differenceX;
        cursorMoveY += differenceY;

        if (Math.abs(cursorMoveY) + v.snapPositionY > v.snapPositionY + config.snappingDistance ||
            Math.abs(cursorMoveX) + v.snapPositionX > v.snapPositionX + config.snappingDistance) {
            v.currentBox.snap = false;
            m.setCurrentBoxXY(v.snapPositionX, v.snapPositionY);
            m.setCurrentBoxSnappingDistance(v.snapPositionX, v.snapPositionY);
            cursorMoveX = 0;
            cursorMoveY = 0;
        }
    }

    mouseX = e.pageX;
    mouseY = e.pageY;
}

export default {
    tryToCatchBox,
    tryToDropBox,
    moveBox
}